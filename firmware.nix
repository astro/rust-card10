{ pkgs ? import <nixpkgs> {},
  src ? ./.,
  srcPath ? "card10-sys/firmware",
}:
with pkgs;

let
  cSrc = stdenv.mkDerivation rec {
    name = "card10-src";
    inherit src;
    phases = [ "unpackPhase" "patchPhase" "installPhase" ];
    nativeBuildInputs = [ git ];
    prePatch = "cd ${srcPath}";
    patches = [
      ./0001-hack-epicardium-reduce-init-delay-from-2s-to-0.1s.patch
    ];
    postPatch = ''
      VERSION="$(git -C ${src} describe --always)"
      GITHASH="$(git -C ${src} rev-parse HEAD)"

      substituteInPlace tools/version-header.sh \
        --replace "\$VERSION" "$VERSION" \
        --replace "\$GITHASH" "$GITHASH" \
        --replace "git -C" echo
    '';
    installPhase = ''
      cp -ar . $out

      mkdir -p $out/nix-support
      for f in $out/*.bin ; do
        echo file binary-dist $f >> $out/nix-support/hydra-build-products
      done
    '';
  };
  firmware = lib.makeOverridable (attrs: {
    inherit (attrs) src;
    firmware = import attrs.src;
  }) { src = cSrc; };
in firmware
