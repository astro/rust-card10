{ pkgs ? import <nixpkgs> {},
  cFirmware ? (import ./firmware.nix { inherit pkgs; }).firmware,
  rustL0dables ? (import ./default.nix {}).l0dables,
}:
with pkgs;

stdenv.mkDerivation {
  name = "card10-firmware";
  buildInputs = [ cFirmware rustL0dables ];
  phases = [ "installPhase" ];
  installPhase = ''
    mkdir $out
    cp -r ${cFirmware}/card10/* $out/

    chmod u+w $out/apps
    cp ${rustL0dables}/apps/* $out/apps/

    cat << EOF > $out/card10.cfg
    execute_elf=true
    EOF
  '';
}
