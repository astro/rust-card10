{ rustManifest ? ./channel-rust-nightly.toml,
}:
let
  mozillaOverlay = import <mozillaOverlay>;
  manifestOverlay = self: super: {
    rustChannelOfTargets = _channel: _date: targets:
      (super.lib.rustLib.fromManifestFile rustManifest {
        inherit (super) stdenv fetchurl patchelf;
      }).rust.override { inherit targets; };
  };
  pkgs = (import <nixpkgs> { overlays = [ mozillaOverlay manifestOverlay ]; });
  project = import ../default.nix {
    inherit pkgs mozillaOverlay;
    firmwareSrc = <firmware>;
  };
  l0dable = crate: project.l0dables.overrideAttrs (oldAttrs: {
    name = "${oldAttrs.name}-${crate}";
    buildPhase = ''
      pushd ${crate}
      ${oldAttrs.buildPhase}
      popd
    '';
    installPhase = ''
      ${oldAttrs.installPhase}

      mkdir -p $out/nix-support
      for f in $out/apps/*.elf ; do
        echo file binary-dist $f >> $out/nix-support/hydra-build-products
      done
    '';
  });
  crates =
    pkgs.lib.filterAttrs (crate: type:
      type == "directory" &&
      builtins.pathExists (<rust-card10> + "/${crate}/Cargo.toml")
    ) (builtins.readDir <rust-card10>);
in
builtins.mapAttrs (crate: type:
  pkgs.lib.hydraJob (l0dable crate)
) crates
