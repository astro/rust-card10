//! Lights

use card10_sys::*;

#[derive(Clone, Copy)]
pub enum LEDColor {
    RGB(u8, u8, u8),
    HSV(f32, f32, f32),
}

/// Update all RGB LEDs
///
/// `f` must supply a `LEDColor` for `0..=10`.
pub fn update_rgb_leds<F>(f: F)
where
    F: Fn(i32) -> LEDColor,
{
    for index in 0..=10 {
        let color = f(index);
        match color {
            LEDColor::RGB(r, g, b) => unsafe {
                epic_leds_prep(index, r, g, b);
            }
            LEDColor::HSV(h, s, v) => unsafe {
                epic_leds_prep_hsv(index, h, s, v);
            }
        }
        unsafe {
            epic_leds_update();
        }
    }
}

/// Set powersave mode for the RGB leds
///
/// Setting powersave mode disables the LEDs, saving
/// ~15 mA
pub fn set_rgb_leds_powersave(v: bool) {
    unsafe { epic_leds_set_powersave(v); }
}

#[repr(i32)]
pub enum Rocket {
    Blue = 0,
    Yellow = 1,
    Green = 2,
}

/// Set one of the rocket LEDs to the desired brightness.
///
/// Brightness should be 0 - 31; larger values will be clamped.
pub fn set_rocket(r: Rocket, brightness: u8) {
    let v = if brightness > 31{31} else {brightness}; // clamp
    unsafe { epic_leds_set_rocket(r as i32, v) }
}
